# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-01-02 00:04+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:16
msgid "Position and Zoom"
msgstr "Positie en zoom"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:18
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:20
msgid ""
"Adjust size and position of clip using smooth affine transformations. "
"Formerly known as \"Pan and Zoom\"."
msgstr ""
"Grootte en positie van clip aanpassen met soepele affine-transformaties. "
"Eerder bekend als \"Rondkijken en zoomen\"."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:22
msgid ""
"In this example we have two keyframes in the pan and zoom, one at the "
"beginning and one at the end. Size is 25% at the start keyframe and 100% at "
"the end. The images are centered on the screen at both keyframes."
msgstr ""
"In dit voorbeeld hebben we twee keyframes in het rondkijken en zoomen, een "
"aan het begin en een aan het eind. Grootte is 25% bij het keyframe aan het "
"begin en 100% bij het eind. De afbeeldingen zijn gecentreerd op het scherm "
"op beide keyframes."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:24
msgid "https://youtu.be/0aSe1y6e4RE"
msgstr "https://youtu.be/0aSe1y6e4RE"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:26
msgid "See also this :ref:`blue_screen` that describes how to use:"
msgstr "Zie ook dit :ref:`blue_screen` die het hoe te gebruiken beschrijft:"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:28
msgid "Alpha Manipulation -> :ref:`blue_screen`"
msgstr "Alfa-manipulatie -> :ref:`blue_screen`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:29
msgid ":ref:`rotoscoping`"
msgstr ":ref:`rotoscoping`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:30
msgid ":ref:`composite`"
msgstr ":ref:`composite`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:31
msgid "Crop and Transform -> Pan and Zoom effect"
msgstr "Afsnijden en transformatie -> Rondkijk en zoomeffect"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:32
msgid "Enhancement -> :ref:`sharpen`"
msgstr "Verbetering -> :ref:`sharpen`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:33
msgid "Alpha Manipulation -> :ref:`alpha_operations`"
msgstr "Alfa-manipulatie -> :ref:`alpha_operations`"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/pan_and_zoom.rst:35
msgid ""
"`Tutorial: How to do pan and zoom with Kdenlive video editor - Peter "
"Thomson(YouTube) <https://youtu.be/B8ZPoWaxQrA>`_"
msgstr ""
"`Handleiding: Hoe rondkijken en zoomen met Kdenlive videobewerker doen - "
"Peter Thomson(YouTube) <https://youtu.be/B8ZPoWaxQrA>`_"
