# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-17 11:50+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../user_interface/menu/clip_menu/transcode.rst:14
msgid "Transcode Menu Item"
msgstr ""

#: ../../user_interface/menu/clip_menu/transcode.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../user_interface/menu/clip_menu/transcode.rst:18
msgid ""
"From right-click on a clip in :ref:`project_tree`, one of the items that "
"appears in the menu is the :menuselection:`Transcode` submenu."
msgstr ""

#: ../../user_interface/menu/clip_menu/transcode.rst:22
msgid ""
"Choose a transcode profile from the available list to transcode the selected "
"clip into a different video format. The options are controlled by :ref:"
"`configure_kdenlive`. The transcoding is done by the `ffmpeg <http://www."
"ffmpeg.org/>`_ program."
msgstr ""

#: ../../user_interface/menu/clip_menu/transcode.rst:26
msgid ""
"The above screenshot is the dialog presented after choosing a transcode "
"profile for transcoding the clip. The wrench icon toggles the display of the "
"details of the command that will be used for transcoding. The description "
"comes from the description supplied in the :ref:`configure_kdenlive` for "
"this functionality."
msgstr ""

#: ../../user_interface/menu/clip_menu/transcode.rst:28
msgid ""
"Use the checkbox to cause the transcoded clip to be added to the Project Bin "
"once the transcode job has finished."
msgstr ""

#: ../../user_interface/menu/clip_menu/transcode.rst:30
msgid ""
"While the transcode job is running, the Project Bin will display a progress "
"bar on the thumbnail of the clip and a job list menu item will appear at the "
"top of the Project Bin."
msgstr ""
