msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2023-03-11 04:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_glossary___useful_resources.pot\n"
"X-Crowdin-File-ID: 25889\n"

#: ../../glossary/useful_resources.rst:17
msgid "Useful Resources"
msgstr "实用资源"

#: ../../glossary/useful_resources.rst:20
msgid "Contents"
msgstr "目录"

#: ../../glossary/useful_resources.rst:22
msgid ""
"Another Kdenlive manual: `flossmanuals <http://www.flossmanuals.net/how-to-"
"use-video-editing-software/>`_"
msgstr ""
"另一个 Kdenlive 手册：`flossmanual <http://www.flossmanuals.net/how-to-use-"
"video-editing-software/>`_"

#: ../../glossary/useful_resources.rst:23
msgid ""
"`Cutting and Splicing Video in KDEnlive <http://www.linuceum.com/Desktop/"
"KDEnliveVideo.php>`_  by Linuceum"
msgstr ""
"`在 KDEnlive  中剪辑视频 <http://www.linuceum.com/Desktop/KDEnliveVideo."
"php>`_ 由 Linuceum 撰写"

#: ../../glossary/useful_resources.rst:24
msgid ""
"`opensource.com tutorial <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_"
msgstr ""
"`opensource.com 教程 <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_"

#: ../../glossary/useful_resources.rst:25
msgid "`Kdenlive Forum <https://forum.kde.org/viewforum.php?f=262>`_"
msgstr "`Kdenlive 论坛 <https://forum.kde.org/viewforum.php?f=262>`_"

#: ../../glossary/useful_resources.rst:26
msgid ""
"Kdenlive `Developer Wiki <https://community.kde.org/Kdenlive/Development>`_"
msgstr ""
"Kdenlive `开发者维基 <https://community.kde.org/Kdenlive/Development>`_"

#: ../../glossary/useful_resources.rst:29
msgid "Keyboard Stickers - courtesy of Weevil"
msgstr "键盘贴纸 - 由 Weevil 制作"

#: ../../glossary/useful_resources.rst:32
msgid "|Kdenlive_Keyboard_074|"
msgstr "|Kdenlive_Keyboard_074|"

#: ../../glossary/useful_resources.rst:35
msgid "|Kdenlive_Keyboard_074_A4|"
msgstr "|Kdenlive_Keyboard_074_A4|"
