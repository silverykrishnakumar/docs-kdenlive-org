msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-13 00:22+0000\n"
"PO-Revision-Date: 2023-03-11 04:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_user_interface___menu___view_menu___project_monitor.pot\n"
"X-Crowdin-File-ID: 26019\n"

#: ../../user_interface/menu/view_menu/project_monitor.rst:13
msgid "Project Monitor"
msgstr ""

#: ../../user_interface/menu/view_menu/project_monitor.rst:16
msgid "Contents"
msgstr ""

#: ../../user_interface/menu/view_menu/project_monitor.rst:18
msgid "Toggles the display of the :ref:`monitors`."
msgstr ""
