msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2023-03-11 04:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___misc___dance."
"pot\n"
"X-Crowdin-File-ID: 26065\n"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:10
msgid "Dance"
msgstr "Dance - 跳动"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:12
msgid "Contents"
msgstr "目录"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:14
msgid "This effect causes the video frame to dance around the screen."
msgstr "这种效果会让视频的画面在屏幕上跳动。"

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:16
msgid ""
"Add this effect to a video and include another video track below it and a "
"composite transition between the two tracks."
msgstr ""

#: ../../effects_and_compositions/effect_groups/misc/dance.rst:18
msgid "https://youtu.be/gqxU1nvh6JI"
msgstr ""
