# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-24 18:43+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.80\n"

#: ../../effects_and_compositions/effect_groups/artistic/vignette_effect.rst:14
msgid "Vignette Effect"
msgstr "Effet de vignette"

#: ../../effects_and_compositions/effect_groups/artistic/vignette_effect.rst:16
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/artistic/vignette_effect.rst:18
msgid ""
"This is the `Vignette <https://www.mltframework.org/plugins/FilterVignette/"
">`_ MLT filter."
msgstr ""
"Ceci est un filtre « Vignette <https://www.mltframework.org/plugins/"
"FilterVignette/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/artistic/vignette_effect.rst:20
msgid ""
"Vignette around a point with adjustable smoothness, radius, position and "
"transparency."
msgstr ""
"Vignette autour d'un point dont la finesse, le rayon, la position et la "
"transparence sont réglables."

#: ../../effects_and_compositions/effect_groups/artistic/vignette_effect.rst:22
msgid "https://youtu.be/FJrYRD6RzJ4"
msgstr "https://youtu.be/FJrYRD6RzJ4"
