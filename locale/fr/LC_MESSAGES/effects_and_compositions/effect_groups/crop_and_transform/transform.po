# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-30 07:32+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:14
msgid "Transform"
msgstr "Transformer"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:16
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:18
msgid ""
"This is the `Qtblend <https://www.mltframework.org/plugins/FilterQtblend/>`_ "
"MLT filter."
msgstr ""
"Ceci est le filtre « Qtblend <https://www.mltframework.org/plugins/"
"FilterQtblend/> »_ MLT."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:20
msgid "Manipulates Position, scale and opacity."
msgstr "Altère la position, l'échelle et l'opacité."

#: ../../effects_and_compositions/effect_groups/crop_and_transform/transform.rst:22
msgid ""
"The Composition mode parameter of the effect is documented on the Qt "
"documentation under `QPainter CompositionMode <https://doc.qt.io/qt-5/"
"qpainter.html#CompositionMode-enum>`_."
msgstr ""
"Le paramètre « Mode de composition » de l'effet est documenté sur la "
"documentation Qt sous « Mode de composition QPainter<https://doc.qt.io/qt-5/"
"qpainter.html#CompositionMode-enum> »_."
