# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-03 13:51+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:14
msgid "Box Blur"
msgstr "Flou rectangulaire"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:16
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:18
msgid ""
"This is the `Boxblur <https://www.mltframework.org/plugins/FilterBoxblur/>`_ "
"MLT filter."
msgstr ""
"Ceci est le filtre « Boxblur <https://www.mltframework.org/plugins/"
"FilterBoxblur/> »_MLT."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:20
msgid "Separate horizontal and vertical blur."
msgstr "Séparer le flou rectangulaire horizontal et vertical."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:22
msgid ""
"The unit for the boxblur \"hori\" and \"vert\" parameters is \"pixels\". For "
"each pixel in the image, the parameters indicate what pixels will be mixed "
"with it in order to create the blur effect. For example, if you set \"hori\" "
"to 5, then each pixel will be blurred with the sample that is 5 pixels away "
"on each side."
msgstr ""
"L'unité des paramètres « hori » et « vert »pour le filtre « boxblur » est le "
"« pixel ». Pour chaque pixel de l'image, les paramètres indiquent quels "
"pixels seront mélangés avec lui afin de créer l'effet de flou. Par exemple, "
"si vous définissez « hori » à 5, chaque pixel sera flouté avec l'échantillon "
"se trouvant à 5 pixels de chaque côté."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:24
msgid "https://youtu.be/MBO-STDoj_Y"
msgstr "https://youtu.be/MBO-STDoj_Y"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:26
msgid ""
"Not 100% sure if this sample is Box Blur or Square Blur. The caption on this "
"sample was translated from Spanish. Original Spanish was \"Difuminar en "
"cuadro\".*"
msgstr ""
"Il n'est pas sûr à 100% de savoir si cet échantillon est un flou "
"rectangulaire ou en boîte. La légende pour cet échantillon a été traduite de "
"l'espagnol dont l'original espagnol était « Difuminar de cuadro  »*."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/box_blur.rst:28
msgid "https://youtu.be/0Q_BtThC-V0"
msgstr "https://youtu.be/0Q_BtThC-V0"
