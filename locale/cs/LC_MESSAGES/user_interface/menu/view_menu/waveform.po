# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-27 00:38+0000\n"
"PO-Revision-Date: 2022-06-27 16:46+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.2\n"

#: ../../user_interface/menu/view_menu/waveform.rst:13
msgid "Waveform"
msgstr "Křivka"

#: ../../user_interface/menu/view_menu/waveform.rst:16
msgid "Contents"
msgstr "Obsah"

#: ../../user_interface/menu/view_menu/waveform.rst:18
msgid ""
"This data is a 3D histogram.  It represents the Luma component (whiteness) "
"of the video. It is the same type of graph as for the :ref:`rgb_parade`. The "
"horizontal axis represents the horizontal axis in the video frame. The "
"vertical axis is the pixel luma from 0 to 255. The brightness of the point "
"on the graph represents the count of the number of pixels with this luma in "
"this column of pixels in the video frame."
msgstr ""

#: ../../user_interface/menu/view_menu/waveform.rst:26
msgid ""
"For more information see :ref:`Granjow's blog <waveform_and_RGB_parade>` on "
"the waveform and RGB parade scopes. This blog gives some information on how "
"to use the data provided by the RGB parade to do color correction on video "
"footage."
msgstr ""

#: ../../user_interface/menu/view_menu/waveform.rst:30
msgid ":ref:`scopes_directx`"
msgstr ""
