# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-06-27 16:50+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.2\n"

#: ../../user_interface/menu/clip_menu/extract_audio.rst:16
msgid "Extract Audio"
msgstr "Extrahovat audio"

#: ../../user_interface/menu/clip_menu/extract_audio.rst:18
msgid "Contents"
msgstr "Obsah"

#: ../../user_interface/menu/clip_menu/extract_audio.rst:20
msgid ""
"This feature extracts the audio out of a video clip into a :file:`.WAV` file "
"and adds it to the :ref:`project_tree`."
msgstr ""

#: ../../user_interface/menu/clip_menu/extract_audio.rst:22
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin or under the :ref:`clip_menu` when a clip is selected in the "
"Project Bin."
msgstr ""

#: ../../user_interface/menu/clip_menu/extract_audio.rst:28
msgid "The process runs as a job in the Project Bin."
msgstr ""
