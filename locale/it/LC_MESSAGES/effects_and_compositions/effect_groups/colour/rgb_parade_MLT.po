# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-04-02 07:56+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:15
msgid "RGB Parade"
msgstr "Esibizione RGB"

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:17
msgid "Contents"
msgstr "Contenuto"

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:19
msgid ""
"This is the `Frei0r rgbparade <https://www.mltframework.org/plugins/"
"FilterFrei0r-rgbparade/>`_ MLT filter."
msgstr ""
"È il filtro MLT `Frei0r rgbparade <https://www.mltframework.org/plugins/"
"FilterFrei0r-rgbparade/>`_."

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:21
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""
"Nella versione 17.04 si trova nella categoria di effetti :ref:"
"`analysis_and_data`."

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:23
msgid "Displays a histogram of R, G and B components of the video data."
msgstr "Visualizza un istogramma delle componenti R, G e B dei dati video."

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:25
msgid "https://youtu.be/KaxBEhzS8fk"
msgstr "https://youtu.be/KaxBEhzS8fk"

#: ../../effects_and_compositions/effect_groups/colour/rgb_parade_MLT.rst:27
msgid ""
"This is different from the :ref:`rgb_parade` from the View Menu because the "
"Effect version writes the histogram into the output video whereas the View "
"Menu version just displays the histogram widget in the application while you "
"preview your project."
msgstr ""
"È diverso dal :ref:`rgb_parade` del menu Visualizza: la versione effetto "
"infatti scrive l'istogramma sul video in uscita, mentre quella del menu "
"Visualizza mostra soltanto l'oggetto istogramma nell'applicazione durante "
"l'anteprima del progetto."
