# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2022-08-20 19:05+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../effects_and_compositions/transitions/affine.rst:12
#, fuzzy
msgid "Affine Transition"
msgstr "Dodaj prehod"

#: ../../effects_and_compositions/transitions/affine.rst:14
msgid "Contents"
msgstr "Vsebina"

#: ../../effects_and_compositions/transitions/affine.rst:16
#, fuzzy
msgid "Generates image rotation in 3D space, skew and distortion."
msgstr "Ustvarja vrtenje slike v 3D prostoru, skew in izkrivljanje."

#: ../../effects_and_compositions/transitions/affine.rst:18
#, fuzzy
msgid ""
"Provides keyframable animated affine transformations with dissolve "
"functionality."
msgstr ""
"Ponuja ključne obsežne animirane afinske transformacije z funkcionalnostjo "
"raztapljanja."

#: ../../effects_and_compositions/transitions/affine.rst:20
#, fuzzy
msgid ""
"In many applications, this transition can be used instead of a :ref:"
"`composite` and this provides a workaround to the composite transition "
"\"green tinge\" bug reported by some. (Mentioned in legacy Mantis bug "
"tracker ID 2759."
msgstr ""
"V mnogih aplikacijah se ta prehod lahko uporabi namesto :ref:'composite' in "
"to zagotavlja zaodložek sestavljenega prehoda \"green tinge\" bug, ki so ga "
"poročali nekateri. (Omenjeno v zapuščini Mantis bug tracker ID 2759."

#: ../../effects_and_compositions/transitions/affine.rst:24
msgid "Example 1"
msgstr "Primer 1"

#: ../../effects_and_compositions/transitions/affine.rst:26
msgid "https://youtu.be/hylowKurZaw"
msgstr "https://youtu.be/hylowKurZaw"

#: ../../effects_and_compositions/transitions/affine.rst:34
#, fuzzy
msgid "Disolve using Affine Transition"
msgstr "Odstrani izbor prehoda"

#: ../../effects_and_compositions/transitions/affine.rst:36
#, fuzzy
msgid "To add a Dissolve, change the opacity to zero percent."
msgstr "Če želite dodati raztopi, spremenite motnost na nič odstotka."

#: ../../effects_and_compositions/transitions/affine.rst:40
#, fuzzy
msgid "Rotation using Affine Transition"
msgstr "Vrtenje s prehodom Affine"

#: ../../effects_and_compositions/transitions/affine.rst:42
#, fuzzy
msgid ""
"To rotate the image, add a keyframe and enter values for rotation. The units "
"are 10ths of degrees. (e.g. 900 = 90 degree rotation)."
msgstr ""
"Če želite zasukati sliko, dodajte ključni okvir in vnesite vrednosti za "
"vrtenje. Enote so 10 stopinj. (npr. 900 = 90 stopinj vrtenja)."

#: ../../effects_and_compositions/transitions/affine.rst:44
#, fuzzy
msgid ""
"**Rotate X** rotates the frame in the plane of the screen. **Rotate Y** and "
"**Rotate Z** create the illusion of 3D rotation when used dynamically with "
"keyframes - see example below."
msgstr ""
"**Zasukajte X** zavrtite okvir v ravnini zaslona. **Zasukajte Y** in "
"**Zasukajte Z** ustvarite iluzijo 3D vrtenja, ko se uporablja dinamično s "
"ključnimi okvirji - glejte spodnji primer."

#: ../../effects_and_compositions/transitions/affine.rst:47
#, fuzzy
msgid ""
"You can create a similar effect using the :ref:`rotate_(keyframable)` effect "
"from the Crop and Transform group."
msgstr ""
"Podoben učinek lahko ustvarite z učinkom :ref:'rotate_(keyframable)\" iz "
"skupine Obrezovanje in preoblikovanje."

#: ../../effects_and_compositions/transitions/affine.rst:51
msgid "Example 2 - Rotate Y"
msgstr "Primer 2 - Zasukaj Y"

#: ../../effects_and_compositions/transitions/affine.rst:53
msgid "https://youtu.be/IAWMIL7c9K4"
msgstr "https://youtu.be/IAWMIL7c9K4"

#: ../../effects_and_compositions/transitions/affine.rst:55
#, fuzzy
msgid ""
"This example is created using 3 keyframes. The second keyframe is shown "
"below with a **Rotate Y** value of 1800 (=180 degrees). Keyframe one and "
"keyframe three both have **Rotate Y** values of zero."
msgstr ""
"Ta primer je ustvarjen s 3 ključnimi okvirji. Spodaj je prikazan drugi "
"ključni okvir z vrednostjo **Zasukaj Y** 1800 (=180 stopinj). Keyframe ena "
"in keyframe tri imajo **Rotate Y** vrednosti nič."

#: ../../effects_and_compositions/transitions/affine.rst:65
msgid ""
"The difference between **Rotate Y** and **Rotate Z** is that the apparent "
"rotation in **Rotate Y** appears to be around a horizontal axis. The "
"rotation in **Rotate Z** appears to be around a vertical axis."
msgstr ""
"Razlika med **Zavrti Y** in **Zavrti Z** je v tem, da je navidezno vrtenje v "
"**Zavrti Y** okrog vodoravne osi. Vrtenje v **Zavrti Z** se zdi okoli "
"navpične osi."
