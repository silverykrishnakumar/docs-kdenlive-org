# Lithuanian translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-15 00:22+0000\n"
"PO-Revision-Date: 2021-11-15 00:22+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:13
msgid "Disable All Timeline Effects"
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:15
msgid ""
"Did you know that you can **temporarily disable all timeline effects at once?"
"** This may be helpful when you want to do some timeline work, yet some "
"performance heavy effects slow down this work. Alternatively, you may want "
"to consider using Kdenlive’s timeline preview."
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:24
msgid ""
"You’ll find the corresponding option in the main menu :menuselection:"
"`Timeline --> Disable Timeline Effects`. This disables or re-enables all "
"timeline effects, that is, timeline clip effects and track effects."
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:26
msgid ""
"However, please note that prior to Kdenlive 16.08.1, track effects are not "
"properly disabled or re-enabled by :menuselection:`Timeline --> Disable "
"Timeline Effects`."
msgstr ""

#: ../../glossary/useful_information/disable_all_timeline_effects.rst:28
msgid ""
"Please see :ref:`effects_everywhere` about how to temporarily disable bin "
"clip effects."
msgstr ""
