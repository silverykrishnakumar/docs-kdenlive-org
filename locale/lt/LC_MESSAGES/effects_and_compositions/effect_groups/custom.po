# Lithuanian translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 00:21+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../effects_and_compositions/effect_groups/custom.rst:15
msgid "Custom Effects"
msgstr ""

#: ../../effects_and_compositions/effect_groups/custom.rst:17
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/custom.rst:19
msgid ""
"The **Custom Group** in the **Effect List** is where effects appear when you "
"choose :menuselection:`Save Effect` from an effect in the  :ref:`effects`."
msgstr ""
