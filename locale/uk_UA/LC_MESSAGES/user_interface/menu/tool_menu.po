# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-27 00:37+0000\n"
"PO-Revision-Date: 2021-12-27 08:58+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../user_interface/menu/tool_menu.rst:13
msgid "Tool Menu"
msgstr "Меню «Інструменти»"

#: ../../user_interface/menu/tool_menu.rst:15
msgid "Contents"
msgstr "Зміст"

#: ../../user_interface/menu/tool_menu.rst:22
msgid ""
"The options on this menu provide three modes and six tools which affect how "
"operations are performed on clips in the timeline. These same options can "
"also be accessed from :ref:`timeline_edit_modes` and :ref:"
"`timeline_edit_tools`. More details on their usage can be found there."
msgstr ""
"Пункти цього меню керують трьома режимами та шістьма інструментами, які "
"стосуються того, як виконуються дії із кліпами на монтажному столі. Це ті "
"самі пункти, доступ до яких також можна отримати з розділів :ref:"
"`timeline_edit_modes` і :ref:`timeline_edit_tools`. Подробиці можна знайти у "
"відповідному розділі підручника."
