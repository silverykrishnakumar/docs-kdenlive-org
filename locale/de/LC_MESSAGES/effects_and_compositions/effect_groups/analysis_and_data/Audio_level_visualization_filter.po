# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-12 00:40+0000\n"
"PO-Revision-Date: 2022-12-12 09:12+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:1
msgid "Effects in Kdenlive video editor"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:1
msgid ""
"KDE, Kdenlive, effects, audio filter, timeline, documentation, user manual, "
"video editor, open source, free, learn, easy"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:19
msgid "Audio Level Visualization Filter"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:21
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:23
msgid ""
"This is the `audiolevelgraph <https://www.mltframework.org/plugins/"
"FilterAudiolevelgraph/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:25
msgid ""
"An audio visualization filter that draws an audio level meter on the image. "
"(audiolevelgraph)"
msgstr ""

#: ../../effects_and_compositions/effect_groups/analysis_and_data/Audio_level_visualization_filter.rst:27
msgid "This filter is keyframe-able."
msgstr ""
