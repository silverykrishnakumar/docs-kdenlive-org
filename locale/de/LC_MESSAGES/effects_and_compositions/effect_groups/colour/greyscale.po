# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-06-08 01:07+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:14
msgid "Greyscale"
msgstr "Graustufen"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:16
msgid "Contents"
msgstr "Inhalt"

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:18
msgid ""
"This is the `Greyscale <https://www.mltframework.org/plugins/FilterGreyscale/"
">`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:20
msgid "Convert colour image to greyscale."
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:22
msgid "https://youtu.be/5L9KTfJFw80"
msgstr ""

#: ../../effects_and_compositions/effect_groups/colour/greyscale.rst:24
msgid "https://youtu.be/bgNd2bHnvSY"
msgstr ""
