# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-03 00:40+0000\n"
"PO-Revision-Date: 2022-06-08 01:27+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../exporting/rendering.rst:1
msgid "Render out your final Kdenlive video for distributing"
msgstr ""

#: ../../exporting/rendering.rst:1
msgid ""
"KDE, Kdenlive, render, guides, scripts, distribute, documentation, user "
"manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../exporting/rendering.rst:29
msgid "Rendering Using Guides and Rendering Scripts"
msgstr ""

#: ../../exporting/rendering.rst:33
msgid "Purpose"
msgstr ""

#: ../../exporting/rendering.rst:35
msgid ""
"When editing video, time means everything. Especially how long it takes you "
"to edit the clips, project, or footage you are working on. If you ever need "
"to export sequences of your timeline separately, **Kdenlive** offers a great "
"way to do this. You can set guides in your project that establish zones. You "
"then can generate rendering scripts that will export these zones at a later "
"date, during your sleep, or while you hunt and find food. Let's check out "
"how to do this."
msgstr ""

#: ../../exporting/rendering.rst:39
msgid "Picking Sections with Guides"
msgstr ""

#: ../../exporting/rendering.rst:41
msgid ""
"Start by adding a clip into the timeline. I've added some retro footage "
"about airplanes. Cool."
msgstr ""

#: ../../exporting/rendering.rst:48
msgid ""
"Next we want to add a guide for a specific section of the clip on the "
"timeline. You can add a guide by selecting :menuselection:`Timeline` in the "
"menu and then slide down to :menuselection:`Guides` and select :"
"menuselection:`Add Guide` from the menu. Right clicking the top of the "
"timeline also gives you the option :menuselection:`Add Guide`. If you want "
"to, you can also edit the guides you have set by right clicking on the "
"timeline."
msgstr ""

#: ../../exporting/rendering.rst:55
msgid ""
"After selecting this option, a window appears giving you the *Position* of "
"the guide and a field to add a comment. Labeling the guide won't hurt "
"anyone, so I'll name my first guide the extraordinary *Section 1*. A dark "
"blue line appears vertically down through the tracks on your timeline."
msgstr ""

#: ../../exporting/rendering.rst:62
msgid "I'll add a few more guides and then we'll start rendering."
msgstr ""

#: ../../exporting/rendering.rst:69
msgid ""
"The screenshot shows the 6 guides I have put in my project. They chop up the "
"existing clip as I want for my project (that hopefully will become the first "
"hit retro-experimental film...). Now we can export scripts that, when "
"executed, will render these guide zones."
msgstr ""

#: ../../exporting/rendering.rst:73
msgid "Generating Rendering Scripts"
msgstr ""

#: ../../exporting/rendering.rst:75
msgid ""
"Start by clicking on the :guilabel:`Render` button in your toolbar, the one "
"with the red circle surrounded by a white and black ring. You can also "
"select this by going to the :menuselection:`Project --> Render` menu item (:"
"kbd:`Ctrl + Return`)."
msgstr ""

#: ../../exporting/rendering.rst:82
msgid ""
"The new window gives us many choices about how to render our video. Look at "
"the bottom of the window. We need to select the :guilabel:`Guide zone` "
"option. Selecting this will allow us to render our project using the guides "
"we made earlier. Be sure and name the output file to a unique name for each "
"script we will create. Otherwise the scripts will overwrite the different "
"guide zones and not do what you wanted."
msgstr ""

#: ../../exporting/rendering.rst:89
msgid ""
"We now can choose which guides will establish the regions of video we want "
"to export using the pull down menus next to *From* and *to*. I'll cut out "
"the *Beginning* and instead use *section 1* to *Section 1 End*, the guide "
"names I defined earlier."
msgstr ""

#: ../../exporting/rendering.rst:96
msgid ""
"Now I can render this to a file or generate a script that will render this "
"guide zone to a file. Click :menuselection:`Generate Script` and a dialog "
"appears asking you to name the script. **Kdenlive** stores the clips in :"
"file:`/yourhomedirectory/kdenlive/scripts`"
msgstr ""

#: ../../exporting/rendering.rst:103
msgid ""
"After saving the script, the top tab in the window switches to :"
"menuselection:`Scripts`. This lists all the scripts you have generated for "
"the current project."
msgstr ""

#: ../../exporting/rendering.rst:110
msgid ""
"I went ahead and generated 3 scripts based on the guide zones I set up in my "
"timeline. Be sure and keep the :file:`.sh` extension otherwise the rendering "
"script will not be generated."
msgstr ""

#: ../../exporting/rendering.rst:114
msgid "Starting Your Rendering Scripts"
msgstr ""

#: ../../exporting/rendering.rst:116
msgid ""
"Once each script is generated, you need to start each one. You should be in "
"the script tab and see your scripts listed. Start the process by selecting "
"the script and clicking the :guilabel:`Start Script` button. Do this for "
"each script."
msgstr ""

#: ../../exporting/rendering.rst:123
msgid ""
"After clicking each script, you are switched to the *Job Queue* tab. Here "
"you will see what script is being run and how many more are waiting to be "
"run. If you have a large queue, you can take advantage of the nifty checkbox "
"in the bottom left: :guilabel:`Shutdown computer after renderings`"
msgstr ""

#: ../../exporting/rendering.rst:127
msgid "Starting Your Rendering Scripts in a Command Line Terminal"
msgstr ""

#: ../../exporting/rendering.rst:129
msgid ""
"For troubleshooting purposes there could be times that you want to run the "
"render script in a terminal prompt. Rendering in the terminal can produce "
"error logging information that can assist in debugging rendering issues."
msgstr ""

#: ../../exporting/rendering.rst:131
msgid "To render the video in the terminal ..."
msgstr ""

#: ../../exporting/rendering.rst:133
msgid ""
"Note the location where **Kdenlive** has saved the .sh script (see Figure 8)"
msgstr ""

#: ../../exporting/rendering.rst:134
msgid ""
"Open a terminal and change directories to the location of the :file:`.sh` "
"script"
msgstr ""

#: ../../exporting/rendering.rst:135
msgid "execute the :file:`.sh` script"
msgstr ""

#: ../../exporting/rendering.rst:143
msgid "or"
msgstr ""

#: ../../exporting/rendering.rst:151
msgid "Summary"
msgstr ""

#: ../../exporting/rendering.rst:153
msgid ""
"Creating guides can help organize your project while you work on it and when "
"you share it with the world. You can use guides to keep track of areas or to "
"generate rendering scripts that will do the mundane task for you. This "
"feature makes exporting sections of your project quite easy. There are also "
"other ways to take advantage of rendering sections and guide zones using "
"guides. Have fun. Explore!"
msgstr ""

#~ msgid "Contents"
#~ msgstr "Inhalt"
