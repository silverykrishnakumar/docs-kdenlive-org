msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-04-17 00:38+0000\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../troubleshooting/scopes_directx.rst:11
msgid "Windows issue with scopes"
msgstr ""

#: ../../troubleshooting/scopes_directx.rst:16
msgid "All video scopes are working with DirectX."
msgstr ""

#: ../../troubleshooting/scopes_directx.rst:20
msgid ""
"Workaround: Change the backend to OpenGL (:menuselection:`Settings --> "
"OpenGL Backend --> OpenGL`)"
msgstr ""

#: ../../troubleshooting/scopes_directx.rst:22
msgid ""
"If it's still not working go to: :menuselection:`Help --> Reset "
"Configuration` and try again."
msgstr ""
