# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-01-02 20:44+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../effects_and_compositions/transitions/screen.rst:13
msgid "Transitions - Screen"
msgstr "Övertoningar: Skärm"

#: ../../effects_and_compositions/transitions/screen.rst:15
msgid "Contents"
msgstr "Innehåll"

#: ../../effects_and_compositions/transitions/screen.rst:17
msgid ""
"This is very much like :ref:`blue_screen` but it works with footage filmed "
"against a black background."
msgstr ""
"Den är mycket lik :ref:`blue_screen` men fungerar med film tagen mot en "
"svart bakgrund."

#: ../../effects_and_compositions/transitions/screen.rst:19
msgid "Timeline showing how to apply the \"Screen\" transition."
msgstr "Tidslinje som visar hur övertoningen \"Skärm\" används."

#: ../../effects_and_compositions/transitions/screen.rst:25
msgid ""
"This video composites a video of fire filmed on a black background into "
"another bit of footage using the *Screen* transition."
msgstr ""
"Den här videon sammanfogar en video av eld filmad mot en svart bakgrund med "
"en annan filmsnutt genom att använda övertoningen *Skärm*"

#: ../../effects_and_compositions/transitions/screen.rst:27
msgid "https://youtu.be/GkFdHcf9jbY"
msgstr "https://youtu.be/GkFdHcf9jbY"
