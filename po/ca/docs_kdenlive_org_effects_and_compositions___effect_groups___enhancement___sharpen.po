# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___enhancement___sharpen.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-06 00:38+0000\n"
"PO-Revision-Date: 2022-03-22 21:22+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:14
msgid "Sharpen"
msgstr "Aguditza"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:16
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:18
msgid ""
"The sharpen tool is a port of `unsharp mask <https://en.wikipedia.org/wiki/"
"Unsharp_masking>`_ from Mplayer. The parameters are the usual ones for "
"unsharp masking. The \"size\" means the size of the blur, and the amount is "
"how much of the blurred version gets subtracted."
msgstr ""
"L'eina aguditza és una adaptació de la `màscara unsharp <https://en."
"wikipedia.org/wiki/Unsharp_masking>`_ des de l'Mplayer. Els paràmetres són "
"els habituals per a l'emmascarament poc nítid. La «mida» significa la mida "
"del difuminat i la «quantitat» és la quantitat de la versió borrosa que es "
"resta."

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:20
msgid ""
"The Size parameter ranges from 0 to 1000, where 0 represents a 3x3 "
"neighbourhood, 250 represents a 5x5 neighbourhood, and 1000 represents an "
"11x11 neighbourhood. Typically values from 0-250 are a good starting point."
msgstr ""
"L'interval del paràmetre Mida oscil·la entre 0 i 1000, on 0 representa un "
"escombrat de 3x3, 250 representa un escombrat de 5x5 i 1000 representa un "
"escombrat de 11x11. Normalment, els valors de 0 a 250 són un bon punt de "
"partida."

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:22
msgid ""
"The Amount parameter also ranged from 0 to 1000, and defaults to an input "
"value of 300. The default value of 300 represents a neutral input, "
"corresponding to a 0% sharpening. Increasing above 300 will increase the "
"sharpening amount, with 500 representing 100% sharpening, 700 representing "
"200% sharpening, and 1000 representing 350% sharpening. A value of 500 (ie. "
"100% sharpening) is typically a good place to start. Reducing the input to "
"below 300 will result in a gaussian blurring of the picture, with an input "
"value of 0 representing a -150% \"sharpening\" (ie. gaussian blurring)."
msgstr ""
"El paràmetre Quantitat també oscil·la entre 0 i 1000. El valor predeterminat "
"de 300 representa una entrada neutra, el qual correspon amb una nitidesa del "
"0%. Augmentar per sobre de 300 farà augmentar la quantitat de la nitidesa, "
"amb 500 que representen un 100% de nitidesa, 700 que representen un 200% de "
"nitidesa i 1000 que representen un 350% de nitidesa. Un valor de 500 (és a "
"dir, 100% de nitidesa) sol ser un bon lloc per començar. Reduir l'entrada "
"per sota de 300 donarà lloc a un desenfocament gaussià de la imatge, amb un "
"valor d'entrada de 0 que representa un -150% «de nitidesa» (és a dir, "
"desenfocament gaussià)."

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:24
msgid "See this :ref:`blue_screen` that describes how to use."
msgstr "Vegeu :ref:`blue_screen`, el qual descriu com emprar-lo."

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:26
msgid "Alpha Manipulation -> :ref:`blue_screen`"
msgstr "Manipulació alfa -> :ref:`blue_screen`"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:27
msgid ":ref:`rotoscoping`"
msgstr ":ref:`rotoscoping`"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:28
msgid ":ref:`composite`"
msgstr ":ref:`composite`"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:29
msgid "Crop and Transform -> :ref:`pan_and_zoom`"
msgstr "Retalla i transforma -> :ref:`pan_and_zoom`"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:30
msgid "Sharpen Effect"
msgstr "Efecte aguditza"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:31
msgid "Alpha Manipulation -> :ref:`alpha_operations`"
msgstr "Manipulació alfa -> :ref:`alpha_operations`"

#: ../../effects_and_compositions/effect_groups/enhancement/sharpen.rst:33
msgid "https://youtu.be/l43Hz7YEcYU"
msgstr "https://youtu.be/l43Hz7YEcYU"
