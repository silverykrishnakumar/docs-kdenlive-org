# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___audio_correction___gain.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-28 00:22+0000\n"
"PO-Revision-Date: 2021-11-28 04:36+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/gain.rst:11
msgid "Gain"
msgstr "Guany"

#: ../../effects_and_compositions/effect_groups/audio_correction/gain.rst:13
msgid "Contents"
msgstr "Contingut"

#: ../../effects_and_compositions/effect_groups/audio_correction/gain.rst:15
msgid ""
"An effect to increase or decrease the volume of a clip without the use of "
"keyframes."
msgstr ""
"Un efecte per a fer augmentar o disminuir el volum d'un clip sense l'ús de "
"fotogrames clau."

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/audio_correction/gain.rst:17
msgid ""
"The effect changes volume it terms of percentage, not decibel like the "
"effect `Volume (Keyframable) <https://userbase.kde.org/Kdenlive/Manual/"
"Effects/Audio_Correction/Volume_(keyframable)>`_."
msgstr ""
"L'efecte canvia de volum en termes de percentatge, no en decibels com "
"l'`efecte «Volum (amb fotogrames clau)» <https://userbase.kde.org/Kdenlive/"
"Manual/Effects/Audio_Correction/Volume_(keyframable)>`_."
