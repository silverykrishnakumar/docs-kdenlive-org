# Translation of docs_kdenlive_org_effects_and_compositions___effect_groups___blur_and_hide___auto_mask.po to Catalan
# Copyright (C) 2021 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-06 00:38+0000\n"
"PO-Revision-Date: 2021-12-06 14:04+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:16
msgid "Auto Mask"
msgstr "Màscara automàtica"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:18
msgid "Use :ref:`motion_tracker` instead"
msgstr "En el seu lloc empra :ref:`motion_tracker`"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:21
msgid "Contents"
msgstr "Contingut"

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:23
msgid ""
"This effect can be used to mask peoples faces. It uses motion estimation to "
"track subjects and mask faces. It is the `AutotrackRectangle <https://www."
"mltframework.org/docs/FilterAutotrackRectangleDiscussion/>`_ MLT filter."
msgstr ""
"Aquest efecte es pot utilitzar per a emmascarar les cares de les persones. "
"Utilitza l'estimació del moviment per a fer un seguiment dels subjectes i "
"emmascarar les cares. És el `filtre «AutotrackRectangle» <https://www."
"mltframework.org/docs/FilterAutotrackRectangleDiscussion/>`_ del MLT."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:26
msgid "Demo"
msgstr "Demostració"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:28
msgid "https://youtu.be/rRg_i5C8_Hc"
msgstr "https://youtu.be/rRg_i5C8_Hc"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:31
msgid "How to apply Auto Mask"
msgstr "Com aplicar la Màscara automàtica"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:33
msgid ""
"See video below on how to use this effect. Warning: The effect is not 100% "
"reliable."
msgstr ""
"Vegeu el vídeo a continuació sobre com utilitzar aquest efecte. Avís: "
"l'efecte no és 100% fiable."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:35
msgid "https://youtu.be/ZD0WOsX6B5A"
msgstr "https://youtu.be/ZD0WOsX6B5A"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:38
msgid "Motion Tracking"
msgstr "Seguiment del moviment"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:40
msgid ""
"The Auto Mask effect can also be used to track motion of an object and use "
"it later as keyframes for an effect / transition."
msgstr ""
"L'efecte de Màscara automàtica també es pot utilitzar per a fer un seguiment "
"del moviment d'un objecte i utilitzar-ho més endavant com a fotograma clau "
"per a un efecte / transició."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:44
msgid ""
"The method described here is a re-purposing of the motion tracking data that "
"the Auto Mask effect calculates. You do not need to follow the method "
"described below to generate an Auto Mask that will obscure faces. The "
"instructions in the above video should be enough. Nor can you use the method "
"described below to improve the tracking of the mask created by the Auto Mask "
"effect."
msgstr ""
"El mètode que es descriu aquí és una nova finalitat de les dades de "
"seguiment del moviment que calcula l'efecte Màscara automàtica. No cal que "
"seguiu el mètode que es descriu a continuació per a generar una Màscara "
"automàtica que enfosquirà les cares. N'hi hauria d'haver prou amb les "
"instruccions del vídeo anterior. Tampoc podeu utilitzar el mètode descrit a "
"continuació per a millorar el seguiment de la màscara creat per l'efecte "
"Màscara automàtica."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:46
msgid ""
"To use this feature, first, add the clip you want to analyze in the "
"timeline, and add the \"Auto Mask\" effect to it - Figure 1."
msgstr ""
"Per a utilitzar aquesta característica, primer afegiu a la línia de temps el "
"clip que voleu analitzar i afegiu-hi l'efecte «Màscara automàtica» -figura "
"1-."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:53
msgid ""
"Go to the first frame where your object is visible, and adjust the yellow "
"rectangle so that it surrounds the object, like the hand in Figure 1."
msgstr ""
"Aneu al primer fotograma on el vostre objecte és visible i ajusteu el "
"rectangle groc perquè l'envolti, com la mà en la figura 1."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:55
msgid ""
"Then press the :guilabel:`Analyse` button in the effect options. This will "
"start an analysis of the clip (you can follow its progress in the Project "
"Bin view)."
msgstr ""
"A continuació, premeu el botó :guilabel:`Analitza` a les opcions de "
"l'efecte. Això iniciarà una anàlisi del clip (podreu seguir el seu progrés a "
"la vista Safata del projecte)."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:62
msgid ""
"When the job is finished, the motion tracking data is stored in the clip "
"properties. To use this data, you can, for example, add a title clip and "
"affine transition over the clip you just analyzed, like in the screenshot in "
"Figure 2."
msgstr ""
"Quan s'ha acabat la tasca, les dades de seguiment del moviment "
"s'emmagatzemen en les propietats del clip. Per a utilitzar aquestes dades, "
"podeu, per exemple, afegir un títol del clip i una transició afina sobre el "
"clip que acabeu d'analitzar, com a la captura de pantalla en la figura 2."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:69
msgid ""
"Next step is to import the motion data in the transition. To do this, first, "
"select the clip you have analyzed, then select the transition using the :kbd:"
"`Ctrl` key so that both items are selected. Finally, go in the transitions's "
"Options menu."
msgstr ""
"El pas següent és importar les dades del moviment en la transició. Per a fer-"
"ho, primer seleccioneu el clip que heu analitzat i, a continuació, "
"seleccioneu la transició amb la tecla :kbd:`Ctrl` perquè ambdós elements "
"estiguin seleccionats. Finalment, aneu al menú Opcions de les transicions."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:73
msgid ""
"Select :menuselection:`Import keyframes from clip`. You can now delete the "
"\"Auto Mask\" effect from the clip in the timeline and play the project to "
"see your title clip following the object."
msgstr ""
"Seleccioneu :menuselection:`Importa fotogrames clau des del clip`. Ara podeu "
"suprimir l'efecte «Màscara automàtica» des del clip en la línia de temps i "
"reproduir el projecte per a veure el títol del clip després de l'objecte."

# skip-rule: t-e_sola
#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:80
msgid ""
"Checking the :guilabel:`Limit keyframe number` checkbox in the \"Import "
"Keyframes\" dialog (Figure 4) will cause Kdenlive to only import every nth "
"frame (where n is the number selected in the combo box). This is a useful "
"feature if you want to manually edit the keyframes that are imported because "
"it allows you to limit the number of keyframes you will need to manually "
"edit. If this checkbox is not checked then you import a keyframe for every "
"frame that is in the source clip."
msgstr ""
"Marcar la casella de selecció :guilabel:`Limita el nombre de fotogrames "
"clau` en el diàleg «Importació dels fotogrames clau» (figura 4) farà que el "
"Kdenlive només importi cada e«n»èsim fotograma (on «n» és el nombre "
"seleccionat en el quadre combinat). Aquesta és una característica útil si "
"voleu editar manualment els fotogrames clau que s'importen, perquè permet "
"limitar el nombre de fotogrames clau que s'hauran d'editar manualment. Si "
"aquesta casella de selecció no està marcada, importareu un fotograma clau "
"per a cada fotograma que hi hagi en el clip font."

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:83
msgid "Deleting Motion Tracking Data"
msgstr "Suprimir les dades de Seguiment del moviment"

#: ../../effects_and_compositions/effect_groups/blur_and_hide/auto_mask.rst:85
msgid ""
"The motion tracking data is saved with the :ref:`clips`. You can view this "
"data from the clip properties Analysis tab - Figure 5. Delete the data using "
"button 1."
msgstr ""
"Les dades de seguiment del moviment es desen amb els :ref:`clips`. Podreu "
"veure aquestes dades des de la pestanya Anàlisi de les propietats del clip -"
"figura 5-. Suprimiu les dades amb el botó 1."
