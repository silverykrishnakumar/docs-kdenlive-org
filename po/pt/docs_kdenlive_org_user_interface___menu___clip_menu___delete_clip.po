# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2021-11-22 17:21+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Clip ref projecttree clip clipmenu\n"

#: ../../user_interface/menu/clip_menu/delete_clip.rst:16
msgid "Delete Clip"
msgstr "Apagar 'Clip'"

#: ../../user_interface/menu/clip_menu/delete_clip.rst:18
msgid "Contents"
msgstr "Conteúdo"

#: ../../user_interface/menu/clip_menu/delete_clip.rst:20
msgid ""
"This menu item is available from :ref:`project_tree` on a clip in the "
"Project Bin or under the :ref:`clip_menu` when a clip is selected in the "
"Project Bin."
msgstr ""
"Este item do menu está disponível no :ref:`project_tree` de um 'clip' no "
"Grupo do Projecto ou no :ref:`clip_menu` quando seleccionar um 'clip' no "
"Grupo do Projecto."

#: ../../user_interface/menu/clip_menu/delete_clip.rst:22
msgid ""
"This function removes the clip from the Project Bin and from the timeline if "
"it is being used on the timeline."
msgstr ""
"Esta função remove o 'clip' do Grupo do Projecto e da linha temporal, caso "
"esteja a ser usada na linha temporal."

#: ../../user_interface/menu/clip_menu/delete_clip.rst:24
msgid "You are warned if the clip is in use on the timeline."
msgstr ""
"Irá receber um aviso se o 'clip' estiver a ser usado na linha temporal."
