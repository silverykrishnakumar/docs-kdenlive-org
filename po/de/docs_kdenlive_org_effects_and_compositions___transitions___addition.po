# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-06-08 01:23+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/transitions/addition.rst:11
msgid "addition transition"
msgstr ""

#: ../../effects_and_compositions/transitions/addition.rst:13
msgid "Contents"
msgstr "Inhalt"

#: ../../effects_and_compositions/transitions/addition.rst:15
msgid ""
"This is the `Frei0r addition <https://www.mltframework.org/plugins/"
"TransitionFrei0r-addition/>`_ MLT transition."
msgstr ""

#: ../../effects_and_compositions/transitions/addition.rst:17
msgid "Perform an RGB[A] addition operation of the pixel sources."
msgstr ""
