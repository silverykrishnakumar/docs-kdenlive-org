# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-01-03 14:10+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:12
msgid "Clip Menu — Replace Clip"
msgstr "Menu de vidéo - Remplacer la vidéo"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:14
msgid "Contents"
msgstr "Contenu"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:22
msgid ""
"This menu item is available from :ref:`project_tree` a clip in the Project "
"Bin. Replace Clip will allow you to select a different file but keep all of "
"the uses on the timeline. This can be useful if you work first with "
"placeholder clips (i.e. low resolution) and on the end, before rendering, "
"you replace the clip with the final clip."
msgstr ""
"Cet élément de menu est disponible à partir de :ref:`arborescence_projet` "
"d'une vidéo dans le dossier du projet. La fonction « Remplacer une vidéo » "
"vous permettra de sélectionner un fichier différent tout en conservant "
"toutes les utilisations sur la frise chronologique. Cela peut être utile si "
"vous travaillez d'abord avec des vidéos de remplacement (c'est-à-dire à "
"basse résolution) et qu'à la fin, avant le rendu, vous remplacez la vidéo "
"par la vidéo finale."
