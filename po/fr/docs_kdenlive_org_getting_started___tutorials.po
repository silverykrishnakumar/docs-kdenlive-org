# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2022-01-17 16:56+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.1\n"

#: ../../getting_started/tutorials.rst:1
msgid "Kdenlive video editor tutorials"
msgstr "Tutoriel pour le logiciel de montage vidéo Kdenlive"

#: ../../getting_started/tutorials.rst:1
msgid ""
"KDE, Kdenlive, tutorials, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, tutoriels, documentation, manuel d'utilisation, éditeur "
"vidéo, open source, gratuit, apprendre, facile"

#: ../../getting_started/tutorials.rst:30
msgid "Tutorials"
msgstr "Tutoriels"

#: ../../getting_started/tutorials.rst:32
msgid "Contents"
msgstr "Contenu"

#: ../../getting_started/tutorials.rst:35
msgid "Written Tutorials"
msgstr "Tutoriels"

#: ../../getting_started/tutorials.rst:37
msgid "See :ref:`quickstart` for a step-by-step introductory tutorial"
msgstr ""
"Veuillez consulter :ref:`quickstart` pour un tutoriel d'introduction pas à "
"pas."

#: ../../getting_started/tutorials.rst:40
msgid "Contents:"
msgstr "Contenu :"

#: ../../getting_started/tutorials.rst:46
msgid ""
"`Introduction to Kdenlive <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_ by Seth Kenlon"
msgstr ""
"`Introduction à Kdenlive <http://opensource.com/life/11/11/introduction-"
"kdenlive>`_ par Seth Kenlon"

#: ../../getting_started/tutorials.rst:49
msgid ""
"`10 tools for visual effects with Kdenlive <https://opensource.com/"
"life/15/12/10-kdenlive-tools>`_ by Seth Kenlon"
msgstr ""
"`10 outils pour des effets visuels avec Kdenlive <https://opensource.com/"
"life/15/12/10-kdenlive-tools>`_ par Seth Kenlon"

#: ../../getting_started/tutorials.rst:52
msgid ""
"`Basic masking in Kdenlive <https://opensource.com/life/15/11/basic-masking-"
"kdenlive>`_ by Seth Kenlon"
msgstr ""
"`Masquage basique avec Kdenlive <https://opensource.com/life/15/11/basic-"
"masking-kdenlive>`_ par Seth Kenlon"

#: ../../getting_started/tutorials.rst:55
msgid ""
"`Kdenlive Challenge (Multiple Masks &  Tracks) <http://www.ocsmag."
"com/2015/12/22/the-video-editing-challenge-part-i-kdenlive/>`_ by Paul Browns"
msgstr ""
"`Challenge de Kdenlive (Masques et pistes multiples) <http://www.ocsmag."
"com/2015/12/22/the-video-editing-challenge-part-i-kdenlive/>`_ par Paul "
"Browns"

#: ../../getting_started/tutorials.rst:58
msgid "`Wikibooks Kdenlive manual <http://en.wikibooks.org/wiki/Kdenlive>`_"
msgstr ""
"`Manuel Wikibooks de Kdenlive <https://fr.wikibooks.org/wiki/Kdenlive>`_"

#: ../../getting_started/tutorials.rst:64
msgid "Video Tutorials"
msgstr "Tutoriels en vidéo"

#: ../../getting_started/tutorials.rst:66
msgid ""
"`Image and Title Layers Transparency Tutorial - Open Source Bug <https://www."
"youtube.com/watch?v=f6VHlOZutm8>`_"
msgstr ""
"`Tutoriel sur la transparence des calques d'image et de titre - Bogue en "
"logiciel libre <https://www.youtube.com/watch?v=f6VHlOZutm8>`_"

#: ../../getting_started/tutorials.rst:69
msgid ""
"`How to do pan and zoom with Kdenlive video editor -  Peter Thomson <https://"
"www.youtube.com/watch?v=B8ZPoWaxQrA>`_"
msgstr ""
"`Comment faire un panoramique et un zoom avec l'éditeur vidéo Kdenlive - "
"Peter Thomson <https://www.youtube.com/watch?v=B8ZPoWaxQrA>`_"

#: ../../getting_started/tutorials.rst:72
msgid ""
"`Keyframe Animation - Linuceum <https://www.youtube.com/watch?"
"v=M8hC5FbIzdE>`_"
msgstr ""
"`Animation avec trames clé - Linuceum <https://www.youtube.com/watch?"
"v=M8hC5FbIzdE>`_"

#: ../../getting_started/tutorials.rst:75
msgid ""
"`Kdenlive Tutorials by Arkengheist 2.0 <https://www.youtube.com/channel/"
"UCtkSBZ0x71aeHmR3NNBTWwg>`_ : Many tutorials including Text effects, "
"Transitions, Timelapse, Animation, Lower Thirds, Rotoscoping, and more."
msgstr ""
"`Tutoriels par Arkengheist 2.0 pour Kdenlive <https://www.youtube.com/"
"channel/UCtkSBZ0x71aeHmR3NNBTWwg>`_ : de nombreux tutoriels comprenant des "
"effets de texte, des transitions, du temps accéléré (Timelapse), des "
"animations, des tiers inférieurs, de la rotoscopie et plus encore."

#: ../../getting_started/tutorials.rst:78
msgid ""
"More videos can be found using a `YouTube search <https://www.youtube.com/"
"results?search_query=kdenlive+tutorials>`_ and on the `Vimeo Kdenlive "
"Tutorial Channel <https://vimeo.com/groups/kdenlivetutorials/videos>`_"
msgstr ""
"D'autres vidéos peuvent être trouvées en utilisant une recherche `YouTube "
"<https://www.youtube.com/results?search_query=kdenlive+tutorials>`_ et sur "
"le `canal de tutoriel Vimeo de Kdenlive <https://vimeo.com/groups/"
"kdenlivetutorials/vidéos>`_"
