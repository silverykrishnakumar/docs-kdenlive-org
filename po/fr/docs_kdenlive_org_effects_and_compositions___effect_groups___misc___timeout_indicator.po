# Xavier Besnard <xavier.besnard@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-12-08 19:33+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.11.90\n"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:12
msgid "Timeout Indicator"
msgstr "Indicateur de délai expiré"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:15
msgid "Contents"
msgstr "Contenu"

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:17
msgid ""
"This is `Frei0r timeout <https://www.mltframework.org/plugins/FilterFrei0r-"
"timeout/>`_ MLT filter by Simon A. Eugster."
msgstr ""
"Ceci est le filtre « Frei0r timeout <https://www.mltframework.org/plugins/"
"FilterFrei0r-timeout/> »_ MLT développé par Simon A. Eugster."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:19
msgid ""
"In ver 17.04 this is found in the :ref:`analysis_and_data` category of "
"Effects."
msgstr ""
"Dans la version 17.04, ceci peut être trouvé dans la catégorie :ref:"
"`analysis_and_data` d'effets."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:21
msgid ""
"This adds a little countdown bar to the bottom right of the video and is "
"available in ver. 0.9.5 of **Kdenlive**."
msgstr ""
"Ceci ajoute une petite barre de compte à rebours en bas à droite de la "
"vidéo. Ceci est disponible dans la version 0.9.5 de **Kdenlive**."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:23
msgid "The settings in this screen shot produced the sample video below."
msgstr ""
"Les paramètres de cette capture d'écran ont produit l'exemple de vidéo ci-"
"dessous."

#: ../../effects_and_compositions/effect_groups/misc/timeout_indicator.rst:27
msgid "https://youtu.be/ry3DLZD_bRc"
msgstr "https://youtu.be/ry3DLZD_bRc"
