# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-01-03 14:08+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../user_interface/menu/clip_menu/duplicate_clip.rst:11
msgid "Clip Menu — Duplicate Clip"
msgstr "Menu des vidéos — Dupliquer une vidéo"

#: ../../user_interface/menu/clip_menu/duplicate_clip.rst:13
msgid "Contents"
msgstr "Contenu"

#: ../../user_interface/menu/clip_menu/duplicate_clip.rst:19
msgid ""
"This menu item is available from :ref:`project_tree` a clip in the Project "
"Bin. Duplicate Clip will add another clip in the project bin from the first "
"clip. This can be useful when applying effects to clips, and allowing you to "
"have the same source file with two different sets of applied effects."
msgstr ""
"Cet élément de menu est disponible à partir de :ref:`arborescence_projet` de "
"la vidéo dans le dossier du projet. La fonction « Dupliquer une vidéo » "
"ajoutera une autre vidéo dans le dossier du projet à partir de la première "
"vidéo. Cela peut être utile lorsque vous appliquez des effets aux vidéos et "
"vous permet d'avoir le même fichier source avec deux ensembles différents "
"d'effets appliqués."
