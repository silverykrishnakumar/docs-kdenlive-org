# Spanish translations for docs_kdenlive_org_getting_started.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2021.
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: docs_kdenlive_org_getting_started\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-16 00:37+0000\n"
"PO-Revision-Date: 2021-11-14 04:29+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../getting_started.rst:1
msgid "Do your first steps with Kdenlive video editor"
msgstr ""

#: ../../getting_started.rst:1
msgid ""
"KDE, Kdenlive, quick start, first steps, documentation, user manual, video "
"editor, open source, free, learn, easy"
msgstr ""

#: ../../getting_started.rst:15
msgid "Getting started"
msgstr ""

#: ../../getting_started.rst:17
msgid "A short overview to start with Kdenlive."
msgstr ""

#: ../../getting_started.rst:19
msgid "Contents:"
msgstr "Contenido:"
