# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 00:38+0000\n"
"PO-Revision-Date: 2021-12-03 08:50+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/transitions/alphaxor.rst:11
msgid "alphaxor transition"
msgstr "Перехід alphaxor"

#: ../../effects_and_compositions/transitions/alphaxor.rst:13
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/transitions/alphaxor.rst:15
msgid ""
"This is the `Frei0r alphaxor <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaxor/>`_ MLT transition."
msgstr ""
"Це перехід MLT `Frei0r alphaxor <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaxor/>`_."

#: ../../effects_and_compositions/transitions/alphaxor.rst:17
msgid "The alpha XOR operation."
msgstr "Дія з прозорістю XOR."

#: ../../effects_and_compositions/transitions/alphaxor.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr "На жовтому кліпі трикутна прозора форма із min=0 і max=618."

#: ../../effects_and_compositions/transitions/alphaxor.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr "На зеленому кліпі прямокутна прозора форма із min=0 і max=1000."

#: ../../effects_and_compositions/transitions/alphaxor.rst:23
msgid "alphaxor is the transition in between."
msgstr "alphaxor є проміжним переходом."
