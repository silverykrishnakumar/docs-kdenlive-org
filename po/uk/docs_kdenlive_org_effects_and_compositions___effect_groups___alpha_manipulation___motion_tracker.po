# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-01 00:38+0000\n"
"PO-Revision-Date: 2022-01-01 09:24+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:10
msgid "Motion Tracker"
msgstr "Стеження за рухом"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:13
msgid "Contents"
msgstr "Зміст"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:16
msgid "What is Motion Tracking?"
msgstr "Що таке «стеження за рухом»?"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:18
msgid ""
"Motion tracking is the process of locating a moving object across time. "
"Kdenlive uses `OpenCV (Open Source Computer Vision Library) <https://opencv."
"org/about/>`_ for motion detection."
msgstr ""
"Стеження за рухом — процес визначення координат рухомого об'єкта з плином "
"часу. У Kdenlive для виявлення руху об'єктів використано `OpenCV (Open "
"Source Computer Vision Library) <https://opencv.org/about/>`_."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:20
msgid "-- `Wikipedia <https://en.wikipedia.org/wiki/Video_tracking>`_"
msgstr "-- `Вікіпедія <https://en.wikipedia.org/wiki/Video_tracking>`_"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:26
msgid "How to track a region of a video?"
msgstr "Як стежити за ділянкою відео?"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:28
msgid "The basic workflow for tracking a region consists of:"
msgstr "Базові принципи роботи для стеження за ділянкою:"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:30
msgid "Select the desired region to track on the Project Monitor."
msgstr "Виберіть бажану ділянку для стеження на панелі монітора проєкту."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:31
msgid "Choose a tracking algorithm."
msgstr "Виберіть алгоритм стеження."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:32
msgid "Click on the Analyse button."
msgstr "Натисніть кнопку «Аналіз»."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:41
msgid ""
"When Analyse is done you can export the keyframes to the clipboard by click "
"on |application-menu| and choose :menuselection:`Copy keyframes to "
"clipboard`. See: :ref:`exchange_keyframes_across_effects`"
msgstr ""
"Після виконання аналізу ви можете експортувати ключові кадри до буфера "
"обміну даними натисканням кнопки |application-menu| з наступним вибором "
"пункту :menuselection:`Копіювати ключові кадри до буфера`. Див. :ref:"
"`exchange_keyframes_across_effects`"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:46
msgid "Tracking algorithms"
msgstr "Алгоритми стеження"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:48
msgid "KCF"
msgstr "KCF"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:50
msgid "**Kernelized Correlation Filters**"
msgstr "**Кореляційні фільтри з ядром**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:52
msgid ""
"**Pros:** Accuracy and speed are both better than MIL and it reports "
"tracking failure better than MIL."
msgstr ""
"**Переваги:** точність і швидкість є вищими за than MIL; повідомлення щодо "
"помилки стеження якісніші за MIL."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:54
msgid "**Cons:** Does not recover from full occlusion."
msgstr "**Недоліки:** не відновлюється після повного закриття об'єкта."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:57
msgid "CSRT"
msgstr "CSRT"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:59
msgid ""
"In the Discriminative Correlation Filter with Channel and Spatial "
"Reliability (DCF-CSR), we use the spatial reliability map for adjusting the "
"filter support to the part of the selected region from the frame for "
"tracking. This ensures enlarging and localization of the selected region and "
"improved tracking of the non-rectangular regions or objects. It uses only 2 "
"standard features (HoGs and Colornames). It also operates at a comparatively "
"lower fps (25 fps) but gives higher accuracy for object tracking."
msgstr ""
"У дискримінаційному кореляційному фільтрі із канальною і просторовою "
"достовірністю (DCF-CSR) ми використовуємо карту просторової достовірності "
"для коригування підтримки фільтром частини позначеної ділянки з кадру для "
"стеження. Це забезпечує збільшення і локалізацію позначеної ділянки і "
"удосконалює стеження за непрямокутними ділянками або об'єктами. Використано "
"лише дві стандартні можливості (HoG і Colornames). Також, працює для "
"порівняно низької частоти кадрів (25 кадрів за секунду), але дає високу "
"точність у стеженні за об'єктами."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:62
msgid "MOSSE"
msgstr "MOSSE"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:64
msgid "**Minimum Output Sum of Squared Error**"
msgstr "**Мінімальна вихідна сума квадратичної похибки**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:66
msgid ""
"MOSSE uses an adaptive correlation for object tracking which produces stable "
"correlation filters when initialized using a single frame. MOSSE tracker is "
"robust to variations in lighting, scale, pose, and non-rigid deformations. "
"It also detects occlusion based upon the peak-to-sidelobe ratio, which "
"enables the tracker to pause and resume where it left off when the object "
"reappears. MOSSE tracker also operates at a higher fps (450 fps and even "
"more)."
msgstr ""
"У MOSSE для стеження за об'єктами використано адаптивну кореляцію, яка дає "
"стабільні фільтри кореляції при ініціалізації з використанням одного кадру. "
"Засіб стеження MOSSE є стійким до варіації освітлення, масштабу, пози та "
"нежорстких деформацій. Він також виявляє закриття об'єкта на основі даних "
"щодо відношення піку до побічного максимуму, що надає змогу засобу стеження "
"призупиняти і відновлювати роботу при повторній появі об'єкта. Крім того, "
"засіб стеження MOSSE працює із вищими частотами кадрів (450 кадрів за "
"секунду і навіть вище)."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:68
msgid "**Pros:** It is as accurate as other complex trackers and much faster."
msgstr ""
"**Переваги:** є таким саме точним, як інші складні засоби стеження, і "
"набагато швидшим за них."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:70
msgid ""
"**Cons:** On a performance scale, it lags behind the deep learning based "
"trackers."
msgstr ""
"**Недоліки:** на шкалі швидкодії від відстає від засобів стеження, які "
"засновано на глибинному навчанні."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:73
msgid "MIL"
msgstr "MIL"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:75
msgid ""
"**Pros:** The performance is pretty good. It does a reasonable job under "
"partial occlusion."
msgstr ""
"**Переваги:** доволі добра швидкодія. Працює для часткового закриття об'єкта."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:77
msgid ""
"**Cons:** Tracking failure is not reported reliably. Does not recover from "
"full occlusion."
msgstr ""
"**Недоліки:** не повідомляє надійно про помилку стеження. Не відновлюється "
"після повного закриття об'єкта."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:80
msgid "MedianFlow"
msgstr "MedianFlow"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:82
msgid ""
"**Pros:** Excellent tracking failure reporting. Works very well when the "
"motion is predictable and there is no occlusion."
msgstr ""
"**Переваги:** чудове звітування про помилки стеження. Працює дуже добре, "
"якщо рух передбачуваний і немає закриття об'єкта."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:84
msgid "**Cons:** Fails under large motion."
msgstr "**Недоліки:** не може працювати, якщо рух є достатньо великим."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:87
msgid "`DaSiam <https://arxiv.org/pdf/1808.06048.pdf>`_"
msgstr "`DaSiam <https://arxiv.org/pdf/1808.06048.pdf>`_"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:89
msgid ""
"The DaSiamRPN visual tracking algorithm relies on deep-learning models to "
"provide extremely accurate results."
msgstr ""
"Алгоритм візуального стеження DaSiamRPN покладається на моделі глибинного "
"навчання і дає надзвичайно точні результати."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:91
msgid ""
"In order to use the DaSiam algorithm you need to download the AI models and "
"place them in:"
msgstr ""
"Щоб скористатися алгоритмом DaSiam, вам слід отримати моделі ШІ і записати "
"їх до"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:93
msgid "**Linux**"
msgstr "**Linux**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:95
msgid "*$HOME/.local/share/kdenlive/opencvmodels*."
msgstr "*$HOME/.local/share/kdenlive/opencvmodels*."

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:97
msgid "Flatpak"
msgstr "Flatpak"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:99
msgid "*$HOME/.var/app/org.kde.kdenlive/data/kdenlive/opencvmodels*"
msgstr "*$HOME/.var/app/org.kde.kdenlive/data/kdenlive/opencvmodels*"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:101
msgid "**Windows**"
msgstr "**Windows**"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:103
msgid "*%AppData%/kdenlive/opencvmodels*"
msgstr "*%AppData%/kdenlive/opencvmodels*"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:105
msgid ""
"Press :kbd:`Win + R` (:kbd:`Windows` key and :kbd:`R` key simultaneously) "
"and copy **%AppData%/kdenlive/**. Then create the folder `opencvmodels`"
msgstr ""
"Натисніть :kbd:`Win + R` (клавіші :kbd:`Windows` і :kbd:`R` одночасно) і "
"скопіюйте **%AppData%/kdenlive/**. Далі, створіть теку `opencvmodels`"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:107
msgid ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_cls1.onnx"
msgstr ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_cls1.onnx"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:108
msgid ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_kernel_r1."
"onnx"
msgstr ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_kernel_r1."
"onnx"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:109
msgid ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_model.onnx"
msgstr ""
"https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/dasiamrpn_model.onnx"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:112
msgid "Frame shape"
msgstr "Форма рамки"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:114
#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:119
msgid "soon"
msgstr "невдовзі"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:117
msgid "Shape color"
msgstr "Колір форми"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:122
msgid "Blur type"
msgstr "Тип розмивання"

#: ../../effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.rst:127
msgid ""
"Four blur types are available: Median blur, Gaussian blur, Pixelate, Opaque "
"fill"
msgstr ""
"Доступні чотири типи розмивання: медіанне розмивання, гаусове розмивання, "
"пікселяція, заповнення непрозорим"

#~ msgid "https://www.dropbox.com/s/rr1lk9355vzolqv/dasiamrpn_model.onnx?dl=1"
#~ msgstr "https://www.dropbox.com/s/rr1lk9355vzolqv/dasiamrpn_model.onnx?dl=1"

#~ msgid ""
#~ "https://www.dropbox.com/s/999cqx5zrfi7w4p/dasiamrpn_kernel_r1.onnx?dl=1"
#~ msgstr ""
#~ "https://www.dropbox.com/s/999cqx5zrfi7w4p/dasiamrpn_kernel_r1.onnx?dl=1"

#~ msgid ""
#~ "https://www.dropbox.com/s/qvmtszx5h339a0w/dasiamrpn_kernel_cls1.onnx?dl=1"
#~ msgstr ""
#~ "https://www.dropbox.com/s/qvmtszx5h339a0w/dasiamrpn_kernel_cls1.onnx?dl=1"
