# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-21 00:22+0000\n"
"PO-Revision-Date: 2022-08-20 19:12+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../glossary/useful_information/library_clips.rst:13
#, fuzzy
msgid "Library clips with image sequences, Titles, Color clips"
msgstr "Izrezki knjižnice s slikovnimi zaporedji, Naslovi, Barvni izrezki"

#: ../../glossary/useful_information/library_clips.rst:17
#, fuzzy
msgid ""
"Kdenlive 16.12.0 will see **further improvements to its library clip "
"expansion** (and, in consequence, to MLT playlist import). Not every "
"Kdenlive user will notice the improved functionality, as it affects only "
"library clips where the same image sequence or title is used multiple times."
msgstr ""
"Kdenlive 16.12.0 bo videl **nadaljnje izboljšave razširitve knjižničnih "
"izrezkov** (in posledično uvoz seznama predvajanja MLT). Izboljšane "
"funkcionalnosti ne bo opazil vsak uporabnik Kdenlive, saj vpliva le na "
"knjižničnih izrezkov, kjer se večkrat uporablja enako zaporedje slike ali "
"naslov."

#: ../../glossary/useful_information/library_clips.rst:19
#, fuzzy
msgid ""
"In particular, if you (re) use the same image sequence clip, title clip, or "
"even color clip multiple times **in the same library clip**, such image "
"sequences and titles  **will only be added once to your project bin**. "
"Before Kdenlive 16.12.0, multiple instances of the same clip in the timeline "
"unfortunately resulted in these clips getting added multiple times to the "
"project bin. To emphasize, this undesired behavior only affected image "
"sequences, titles, and color clips."
msgstr ""
"Če (ponovno) večkrat uporabljate isti izrezek slikovnega zaporedja, naslovni "
"izrezek ali celo barvni izrezek **v istem knjižničnih izrezku**, bodo takšna "
"slikovna zaporedja in naslovi **dodani le enkrat v vaš koš projekta**. Pred "
"Kdenlive 16.12.0 je več primerkov istega posnetka v časovnici žal "
"povzročilo, da so ti posnetki večkrat dodani v projektni regal. Če želite "
"poudariti, je to nezaželeno vedenje vplivalo le na zaporedja slik, naslove "
"in barvne izrezke."

#: ../../glossary/useful_information/library_clips.rst:21
#, fuzzy
msgid ""
"When expanding a library Kdenlive 16.12.0 will now check image sequences, "
"titles, and color clips in the timeline for their content, clip name, and "
"original bin folder location. If there is a match, then such a timeline clip "
"will be added only once to your project bin."
msgstr ""
"Ko razširite knjižnico Kdenlive 16.12.0, bo zdaj preveril zaporedja slik, "
"naslove in barvne izrezke v časovnici za njihovo vsebino, ime izrezka in "
"prvotno mesto mape regala. Če se ujema, bo tak izrezek časovnice dodan le "
"enkrat v vaš projektni regal."

#: ../../glossary/useful_information/library_clips.rst:25
#, fuzzy
msgid ""
"A “library clip” is a clip with the “.mlt” suffix, and in particular, a clip "
"that has been added to your personal Kdenlive library. They show up in "
"Kdenlive’s library pane."
msgstr ""
"\"Knjižni izrezek\" je izrezek s priložnikom \".mlt\", še posebej pa "
"posnetek, ki je bil dodan v vašo osebno Kdenlive knjižnico. Prikažejo se v "
"Kdenlivejevem podoknu knjižnice."

#: ../../glossary/useful_information/library_clips.rst:27
#, fuzzy
msgid ""
"Internally, when you select some clips & transitions from the timeline and "
"add them to Kdenlive’s library, these clips and transitions are stored in "
"the file system in a “.mlt” file (which is an MLT playlist to be precise), "
"and shown in the library widget as a new library clip."
msgstr ""
"Interno, ko izberete nekatere izrezke & prehode iz časovnice in jih dodate k "
"Kdenlive knjižnici, so ti izrezki in prehodi shranjeni v datotečni sistem v "
"datoteki \".mlt\" (ki je MLT seznam predvajanja, da je natančno), in "
"prikazani v knjižnični widget kot nov knjižnični izrezek."

#: ../../glossary/useful_information/library_clips.rst:29
#, fuzzy
msgid ""
"Technically, Kdenlive projects are also MLT playlist files. When you add a "
"clip from the library pane to your project this simply adds the underlying "
"MLT playlist file to your project. But in contrast to other clips, such as "
"an MP4 video, you can “expand” library (that is, MLT playlist) clips to get "
"back the individual clips and transitions inside it."
msgstr ""
"Tehnično, Kdenlive projekti so tudi MLT seznam datotek. Ko v projekt dodate "
"izrezek iz podokna knjižnice, to v projekt preprosto doda datoteko seznama "
"predvajanja MLT. Toda v nasprotju z drugimi posnetki, kot je MP4 video, "
"lahko \"razširite\" knjižnico (to je, MLT seznam predvajanja) izrezke, da bi "
"dobili nazaj posamezne izrezke in prehode znotraj."
