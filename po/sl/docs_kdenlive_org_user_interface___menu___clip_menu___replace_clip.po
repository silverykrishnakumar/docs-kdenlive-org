# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 00:37+0000\n"
"PO-Revision-Date: 2022-08-13 18:47+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:12
msgid "Clip Menu — Replace Clip"
msgstr "Meni Posnetek – Zamenjaj posnetek"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:14
msgid "Contents"
msgstr "Vsebina"

#: ../../user_interface/menu/clip_menu/replace_clip.rst:22
msgid ""
"This menu item is available from :ref:`project_tree` a clip in the Project "
"Bin. Replace Clip will allow you to select a different file but keep all of "
"the uses on the timeline. This can be useful if you work first with "
"placeholder clips (i.e. low resolution) and on the end, before rendering, "
"you replace the clip with the final clip."
msgstr ""
"Ta element menija je na voljo iz :ref:`project_tree` posnetka v posodi "
"projekta. Zamenjaj posnetek vam omogoča, da izberete drugo datoteko, vendar "
"ohranite vse trenutne posege na njem v časovnici. To je lahko koristno, če "
"najprej delate z delovnim gradivom (nizke ločljivosti) in na koncu, preden "
"upodobite, posnetek zamenjate s končnim posnetkom."
