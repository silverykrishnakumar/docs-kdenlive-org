msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-09 00:38+0000\n"
"PO-Revision-Date: 2023-03-11 04:53\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___effect_groups___audio_correction___contrast."
"pot\n"
"X-Crowdin-File-ID: 26539\n"

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:14
msgid "Contrast"
msgstr ""

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:16
msgid "Contents"
msgstr ""

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:18
msgid ""
"This is the `Sox contrast <https://www.mltframework.org/plugins/FilterSox-"
"contrast/>`_ MLT filter."
msgstr ""

#: ../../effects_and_compositions/effect_groups/audio_correction/contrast.rst:20
msgid "Process audio using a SoX effect."
msgstr ""
