# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-27 00:38+0000\n"
"PO-Revision-Date: 2022-03-26 20:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:16
msgid ""
"Using P2 footage from the Panasonic HVX200 on GNU/Linux (tested on Ubuntu)"
msgstr ""
"Användning av P2-filmer från Panasonic HVX200 på GNU/Linux (provat på Ubuntu)"

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:20
msgid ""
"Using footage from P2 cards is easy when you know how! The MXF files on P2 "
"cards cannot be read until you convert them with **mxfsplit**, a part of "
"**FreeMXF**. The conversion is lossless and the resulting files contain both "
"video and audio and can be edited in real time with **Kdenlive** (or "
"**Blender 2.5+**) on most computers made within the last five years or so. "
"Also, **FFMPEG** can read these files. This process is very fast because "
"there is no transcoding and so can be done in the field while shooting just "
"as fast as simply transferring the original P2 files."
msgstr ""
"Att använda film från P2-kort är enkelt när man vet hur! MXF-filerna på P2-"
"kort kan inte läsas innan man konverterar dem med  **mxfsplit**, som ingår i "
"**FreeMXF**. Konverteringen är förlustfri och de resulterande filerna "
"innehåller både video och ljud. De kan redigeras i realtid med **Kdenlive** "
"(eller **Blender 2.5+**) på de flesta datorer tillverkade de senaste fem "
"åren eller så. Dessutom kan **FFMPEG** läsa filerna. Processen är mycket "
"snabb eftersom ingen omkodning görs, och kan därmed göras på fältet under "
"tagningen precis lika snabbt som att helt enkelt överföra P2-originalfilerna."

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:24
msgid "Step One: FreeMXF"
msgstr "Steg ett: FreeMXF"

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:28
msgid ""
"Get the source code for **MFXlib** from `here <http://sourceforge.net/"
"projects/mxflib/>`_."
msgstr ""
"Hämta källkoden för **MFXlib** `härifrån <http://sourceforge.net/projects/"
"mxflib/>`_."

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:31
msgid ""
"Then configure, compile, and install it by running the following code in the "
"directory where you saved the source files:"
msgstr ""
"Konfigurera, kompilera och installera det genom att köra följande kod i "
"katalogen där källkodsfilerna sparades:"

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:42
msgid "This will get **mxfsplit** (part of **mxflib**) working."
msgstr "Det får **mxfsplit** (en del av **mxflib**) att fungera."

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:46
msgid "Step Two: Using mxfsplit"
msgstr "Steg två: Använda mxfsplit"

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:50
msgid ""
"Here is a simple script that can be run in the terminal. It will convert all "
"MXF files in a chosen directory into usable files. Do a search and replace "
"for /source/directory and /destination/directory"
msgstr ""
"Här är ett enkelt skript som går att köra i terminalen. Det konverterar alla "
"MXF-filer i en vald katalog till användbara filer. Sök och ersätt /source/"
"directory och /destination/directory."

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:74
msgid "Conclusion"
msgstr "Avslutning"

#: ../../glossary/shootinghints/using_p2_footage_from_panasonic_hvx200.rst:78
msgid ""
"Now you have a script that can easily prepare footage for editing (e.g. with "
"**Kdenlive** or **Blender**) and for transcoding. **FFMPEG** can be used to "
"transcode the resulting .MXF files to whatever format is preferred. For "
"example, the following code would get the files ready for **Youtube**, "
"**Vimeo**, etc.:"
msgstr ""
"Nu har man ett skript som enkelt kan förbereda film för redigering (t.ex. "
"med **Kdenlive** eller **Blender**) och för omkodning. **FFMPEG** kan "
"användas för att omkoda de resulterande .MXF-filerna till vilket format som "
"önskas. Exempelvis skulle följande kod göra filerna redo för **Youtube**, "
"**Vimeo**, etc.:"
