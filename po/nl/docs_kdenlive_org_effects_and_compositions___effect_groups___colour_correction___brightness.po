# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-22 10:40+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:14
msgid "Brightness"
msgstr "Helderheid"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:18
msgid ""
"This is the `Frei0r brightness filter <https://www.mltframework.org/plugins/"
"FilterFrei0r-brightness/>`_ MLT filter."
msgstr ""
"Dit is het MLT-filter `Frei0r brightness filter <https://www.mltframework."
"org/plugins/FilterFrei0r-brightness/>`_."

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:20
msgid "Adjusts the brightness of a source image."
msgstr "Past de helderheid van een bronafbeelding aan."

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:22
msgid "https://youtu.be/qDZVBPoaEzY"
msgstr "https://youtu.be/qDZVBPoaEzY"

#: ../../effects_and_compositions/effect_groups/colour_correction/brightness.rst:24
msgid "https://youtu.be/t4SfVod5zCE"
msgstr "https://youtu.be/t4SfVod5zCE"
