# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2021-11-18 13:20+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:14
msgid "tehroxx0r"
msgstr "tehroxx0r"

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:16
msgid "Contents"
msgstr "Inhoud"

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:18
msgid ""
"This is the `Frei0r tehroxx0r <https://www.mltframework.org/plugins/"
"FilterFrei0r-tehroxx0r/>`_ MLT filter."
msgstr ""
"Dit is het MLT-filter `Frei0r tehroxx0r <https://www.mltframework.org/"
"plugins/FilterFrei0r-tehroxx0r/>`_."

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:20
msgid "Something videowall-ish."
msgstr "Op een videowal lijkend."

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:22
msgid ""
"The effect might not show up during preview but it does appear in the "
"rendered file."
msgstr ""
"Het effect kan niet verschijnen in het voorbeeld maar het verschijnt in het "
"gerenderde bestand."

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:24
msgid ""
"This effect has one parameter – *interval* – with a range from zero to 1. "
"This parameter controls the number of small video frames which appear around "
"the border and how frequently they flash. Higher number = fewer frames and "
"slower flashing."
msgstr ""
"Dit effect heeft één parameter – *interval* – met een reeks van nul tot 1. "
"Deze parameter bestuurt het aantal kleine videoframes die rond de rand "
"verschijnen en hoe vaak ze opflitsen. Hoger getal = minder frames en "
"langzamer opflitsen."

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:26
msgid "https://youtu.be/qyv15F834h4"
msgstr "https://youtu.be/qyv15F834h4"

#: ../../effects_and_compositions/effect_groups/misc/tehroxx0r.rst:28
msgid "https://youtu.be/ii47tIsYFHQ"
msgstr "https://youtu.be/ii47tIsYFHQ"
