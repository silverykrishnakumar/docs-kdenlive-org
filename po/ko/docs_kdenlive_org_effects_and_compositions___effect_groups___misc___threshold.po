# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 00:21+0000\n"
"PO-Revision-Date: 2022-05-08 16:20+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/misc/threshold.rst:13
msgid "Threshold"
msgstr "임곗값"

#: ../../effects_and_compositions/effect_groups/misc/threshold.rst:15
msgid "Contents"
msgstr "목차"

#: ../../effects_and_compositions/effect_groups/misc/threshold.rst:17
msgid ""
"This is the `Threshold <https://www.mltframework.org/plugins/FilterThreshold/"
">`_ MLT filter."
msgstr ""
"`Threshold <https://www.mltframework.org/plugins/FilterThreshold/>`_ MLT 필터"
"입니다."

#: ../../effects_and_compositions/effect_groups/misc/threshold.rst:19
msgid "Make monochrome clip."
msgstr "단색 클립을 만듭니다."

#: ../../effects_and_compositions/effect_groups/misc/threshold.rst:21
msgid "Different to :ref:`threshold0r`."
msgstr ":ref:`threshold0r` 효과와는 다릅니다."
