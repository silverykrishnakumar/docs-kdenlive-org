# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-03-01 00:38+0000\n"
"PO-Revision-Date: 2022-05-08 16:31+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:13
msgid "Audio Spectrum"
msgstr "오디오 스펙트럼"

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:18
msgid "Contents"
msgstr "목차"

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:20
msgid ""
"This allows you to monitor the audio properties of your clip in detail. The "
"graph only display data while the clip is playing in the clip or project "
"monitor."
msgstr ""

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:23
msgid ""
"It graphs the loudness of the audio (in decibels - vertical axis) for each "
"audio frequency (horizontal axis) in the current frame. The blue curve is +- "
"the maximum over the previous few samples."
msgstr ""

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:26
msgid ""
"See also :ref:`spectrogram` scope which displays a graphical representation "
"of the audio spectrum over the entire clip."
msgstr ""

#: ../../user_interface/menu/view_menu/audio_spectrum.rst:34
msgid ""
"For more information see :ref:`Granjow's blog "
"<audio_spectrum_and_spectrogram>` on Audio Spectrum"
msgstr ""
