# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-02 00:49+0000\n"
"PO-Revision-Date: 2022-03-17 15:41+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:14
msgid "Motion Effects - Speed"
msgstr ""

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:18
msgid "Use :ref:`change_speed` instead"
msgstr ""

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:22
msgid "Contents"
msgstr "Obsah"

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:24
msgid ""
"Make clip play faster or slower. Use of this effect mutes the audio of the "
"clip."
msgstr ""

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:30
msgid ""
"The *Stroboscope* setting defines the number frames the effect skips when "
"playing back. For example, if *Stroboscope* is set to 5 then the effect will "
"only show every fifth frame but will show these frames for five times as "
"long, producing a jumpy, stroboscopic effect."
msgstr ""

#: ../../effects_and_compositions/effect_groups/motion/speed.rst:32
msgid ""
"It has been reported that the **Speed** effect does not work very well on "
"H.264-formatted source video. It is recommended to transcode your source "
"material into the DNxHD format and apply the **Speed** effect to that."
msgstr ""
